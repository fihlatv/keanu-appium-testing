#This script will upload the specified app file to BrowserStack
#It will fetch the unique id (appUrl) of the uploaded app file and use it further for automation test runs

#Requires:
#1. command line arg: platform=ios/android
#2. appFilePath: path of the app file .apk or .app (zipped)
#3. bsUserName
#4. bsToken

#----------------------------START----------------------------
timestampVal=""

timestamp() {
	timestampVal=$(date "+%Y%m%d%H%M%S")
}

timestamp

if [$1==""]
then
#echo "Incomplete Parameters: One of the mandatory parameters are not present"
exit
fi

if [ $1 == "android" ]
then
	appFilePath="Keanu_Android_Automation/resources/Keanu-0.0.4.2-primary-universal-debug.apk"
	appCustomID="keanuandroid$timestampVal"
else
	appFilePath="Keanu_iOS_Automation/resources/"
	appCustomID="keanuios$timestampVal"
fi

#echo AppfilePath:${appFilePath}
#echo AppCustomID:${appCustomID}

response=$(curl -u "${bsUserName}:${bsToken}" -X POST "https://api-cloud.browserstack.com/app-automate/upload" -F "file=@${appFilePath}" -F "data={\"custom_id\": \"${appCustomID}\"}")
#echo Response:$response

appUrl=$(echo $response | jq --raw-output '.app_url')

#export appUrl=$appUrl

echo $appUrl
#----------------------------END----------------------------
#Test Area

#echo $appFilePath
#echo $bsUserName
#echo $bsToken
#echo $appCustomID