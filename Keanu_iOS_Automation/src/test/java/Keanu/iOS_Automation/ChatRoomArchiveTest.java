package Keanu.iOS_Automation;

import java.util.logging.Logger;

import org.apache.log4j.PropertyConfigurator;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.iOS_Automation.Library.AppLibrary;
import keanu.iOS_Automation.Library.TestBase;
import keanu.iOS_Automation.PageObject.AccountSelectionScreen;
import keanu.iOS_Automation.PageObject.AppFooters;
import keanu.iOS_Automation.PageObject.ChatScreen;
import keanu.iOS_Automation.PageObject.DiscoverScreen;
import keanu.iOS_Automation.PageObject.PushScreen;
import keanu.iOS_Automation.PageObject.SignInScreen;
import keanu.iOS_Automation.PageObject.WelcomeScreen;

public class ChatRoomArchiveTest extends TestBase {

	public Logger logger;
	SignInScreen signIn;
	WelcomeScreen wc;
	ChatScreen cs;
	AppFooters footer;
	AccountSelectionScreen as;
	DiscoverScreen disc;

	Integer randInt;
	String roomName;
	String sendMsg;

	@BeforeClass
	public void setUp() throws Exception {
		appLibrary = new AppLibrary("Chat archive Test 1");
		logger = Logger.getLogger("Chat archive Test");
		PropertyConfigurator.configure("Log4j.properties");

		driver = (AppiumDriver<MobileElement>) appLibrary.getDriverInstance();

		signIn = new SignInScreen(appLibrary);
		wc = new WelcomeScreen(appLibrary);
		cs = new ChatScreen(appLibrary);
		footer = new AppFooters(appLibrary);
		as = new AccountSelectionScreen(appLibrary);
		disc = new DiscoverScreen(appLibrary);

		randInt = AppLibrary.randIntDigits(9999, 999999);
		Reporter.log(
				"<h1><Center><Font face=\"arial\" color=\"Orange\">Log Summary</font></Center><h1><table border=\"1\" bgcolor=\"lightgray\">");
	}

	@Test(priority = 1)
	public void createChatRoom() throws Exception {

		// login with user a
		wc.skipToSignIn();
		as.accountSelection(true);
		signIn.signIn("testuser321520", "pass@123456", "https://neo.keanu.im");
		roomName = "New Test Chat room " + randInt;

		// create room
		AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
		cs.closeScreen();
		cs.createChatRoomBySearchingFriend("@testuser425425:neo.keanu.im", roomName);
		cs.verifyChatRoom(roomName);
		appLibrary.resetApp();

	}

	@Test(priority = 2)
	public void AcceptChatRequest() throws Exception {
		wc.skipToSignIn();
		as.accountSelection(true);
		signIn.signIn("testuser425425", "pass123", "https://neo.keanu.im");
		AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
		cs.closeScreen();

		cs.joinChatGroups("testuser321520", "testuser425425");
		cs.verifyChatRoom(roomName);
		appLibrary.resetApp();
	}

	@Test(priority = 3)
	public void archieveRoomFromGroupInfo() throws Exception {
		wc.skipToSignIn();
		as.accountSelection(true);
		signIn.signIn("testuser321520", "pass@123456", "https://neo.keanu.im");
		AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
		cs.closeScreen();

		cs.archiveRoom(roomName, "testuser425425");
		appLibrary.resetApp();
		
		wc.skipToSignIn();
		as.accountSelection(true);
		signIn.signIn("testuser425425", "pass123", "https://neo.keanu.im");
		AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
		cs.closeScreen();
		cs.archiveRoom(roomName, "testuser425425");
	}

}
