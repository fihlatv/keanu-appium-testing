package Keanu.iOS_Automation;

import java.util.logging.Logger;

import org.apache.log4j.PropertyConfigurator;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.iOS_Automation.Library.AppLibrary;
import keanu.iOS_Automation.Library.TestBase;
import keanu.iOS_Automation.PageObject.AccountSelectionScreen;
import keanu.iOS_Automation.PageObject.AppFooters;
import keanu.iOS_Automation.PageObject.ChatScreen;
import keanu.iOS_Automation.PageObject.DiscoverScreen;
import keanu.iOS_Automation.PageObject.PushScreen;
import keanu.iOS_Automation.PageObject.SignInScreen;
import keanu.iOS_Automation.PageObject.WelcomeScreen;

public class ChatScreenSettingTest extends TestBase {

	public Logger logger;
	SignInScreen signIn;
	WelcomeScreen wc;
	ChatScreen cs;
	AppFooters footer;
	AccountSelectionScreen as;
	DiscoverScreen disc;

	Integer randInt;
	String roomName;
	String sendMsg;

	@BeforeClass
	public void setUp() throws Exception {
		appLibrary = new AppLibrary("Chat Leave Test 1");
		logger = Logger.getLogger("Chat Leave Test");
		PropertyConfigurator.configure("Log4j.properties");

		driver = (AppiumDriver<MobileElement>) appLibrary.getDriverInstance();

		signIn = new SignInScreen(appLibrary);
		wc = new WelcomeScreen(appLibrary);
		cs = new ChatScreen(appLibrary);
		footer = new AppFooters(appLibrary);
		as = new AccountSelectionScreen(appLibrary);
		disc = new DiscoverScreen(appLibrary);

		randInt = AppLibrary.randIntDigits(9999, 999999);
		Reporter.log(
				"<h1><Center><Font face=\"arial\" color=\"Orange\">Log Summary</font></Center><h1><table border=\"1\" bgcolor=\"lightgray\">");
	}

	@Test(priority = 1)
	public void createChatRoom() throws Exception {

		// login with user a
		wc.skipToSignIn();
		as.accountSelection(true);
		signIn.signIn("testuser13", "pass123456", "https://neo.keanu.im");
		roomName = "New Test Chat room " + randInt;

		// create room
		AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
		cs.closeScreen();
		cs.createChatRoomBySearchingFriend("testuser20", roomName);
		cs.verifyChatRoom(roomName);

	}

	@Test(priority = 2)
	public void MakeUserModerator() throws Exception {
		cs.navigateToChatRoom(roomName);
		AppLibrary.clickMobileElement(driver, ChatScreen.moreInfoButton);
		cs.makeUserModerator("testuser20");
		cs.RemoveUserAsModerator("testuser20");
	}

	// @Test(priority = 2)
	// public void AcceptChatRequest() throws Exception {
	// wc.skipToSignIn();
	// as.accountSelection(true);
	// signIn.signIn("testuser20", "pass123456", "https://neo.keanu.im");
	// AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
	// cs.closeScreen();
	//
	// cs.joinChatGroups("testuser08", "testuser09");
	// cs.verifyChatRoom(roomName);
	// appLibrary.resetApp();
	// }

	// @Test(priority = 3)
	// public void MakeUserModerator() throws Exception {
	//
	// wc.skipToSignIn();
	// as.accountSelection(true);
	// signIn.signIn("testuser08", "pass123456", "https://neo.keanu.im");
	// AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
	// cs.closeScreen();
	//
	//
	//
	//
	//
	//
	//
	//
	// cs.leaveRoom(roomName, "testuser09");
	// cs.verifyChatRoomAbsence(roomName);
	//
	// appLibrary.resetApp();
	//
	// wc.skipToSignIn();
	// as.accountSelection(true);
	// signIn.signIn("testuser09", "pass123456", "https://neo.keanu.im");
	// AppLibrary.clickMobileElement(driver, PushScreen.skipButton);
	// cs.closeScreen();
	//
	// cs.leaveRoom(roomName, "");
	// cs.verifyChatRoomAbsence(roomName);
	// }
}
