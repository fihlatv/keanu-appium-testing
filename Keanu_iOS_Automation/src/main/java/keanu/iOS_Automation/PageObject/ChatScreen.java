package keanu.iOS_Automation.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.iOS_Automation.Library.AppLibrary;

public class ChatScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String clickOnParticularChatRoom = "xpath://XCUIElementTypeStaticText[@name='Replace']";
	public static String chooseFriendLabel = "xpath://XCUIElementTypeOther[@label='Choose Friends']";
	public static String addButton = "id:Add";

	// choose friend Screen

	public static String cancelButton = "id:Cancel";
	public static String chooseFriendsLabel = "xpath://XCUIElementTypeOther[@name='Choose Friends']";
	public static String doneButton = "id:Done";
	public static String addFriendLogo = "xpath:(//XCUIElementTypeStaticText)[1]";
	public static String addFriendLabel = "id:Add Friend";

	/// Archive and Delete
	public static String deleteChat = "id:Delete";
	public static String archiveChat = "id:Archive";

	public static String activeChat = "id:Active";
	public static String unarchiveChat = "id:Unarchive";

	// chats

	public static String chatRoomUserLabel = "xpath://XCUIElementTypeOther[@name='Replace']";
	public static String done = "xpath://XCUIElementTypeButton[@name='Done']";
	public static String message = "id:user1 joined the room, user2 was invited";
	public static String chatroomAddButton = "xpath:(//XCUIElementTypeButton)[4]";
	public static String audioButton = "xpath:(//XCUIElementTypeButton)[5]";
	public static String chatboxtext = "class:XCUIElementTypeTextView";

	// more info setting
	public static String moreInfoButton = "id:More Info";
	public static String groupSubjecteEditButton = "xpath:(//XCUIElementTypeButton)[2]";
	public static String grouptextField = "xpath:(//XCUIElementTypeAlert[@name='Change room name']//XCUIElementTypeOther)[16]";
	public static String okButton = "xpath://XCUIElementTypeButton[@name='OK']";

	public static String settingLabel = "xpath://XCUIElementTypeOther[@name='Settings']";
	public static String img = "xpath:(//XCUIElementTypeImage)[1]";
	public static String roomTopicEditButton = "xpath:(//XCUIElementTypeButton)[3]";
	public static String addFriendsButton = "id:Add Friends";
	public static String shareButton = "id:Share";
	public static String notificationLabel = "id:Notifications";
	public static String notificationToggle = "xpath://XCUIElementTypeSwitch[contains(@name,'Notifications')]";

	public static String memberslabel = "id:Members";
	public static String userStatus = "id:user1 (You)";
	public static String onlinLabel = "id:online";
	public static String user2Label = "id:user2";
	public static String invitedLabel = "id:Invited";

	//
	public static String chatGroupsList = "xpath://XCUIElementTypeStaticText[contains(@name,'invited')]";
	public static String chatGroups = "xpath://XCUIElementTypeStaticText[@name='user1 invited user2']";
	public static String joinGroup = "xpath://XCUIElementTypeButton[@name='Join']";

	//
	public static String chatRoomName = "id:replace";

	//
	public static String backButton = "xpath:(//XCUIElementTypeButton)[1]";

	// Leave

	public static String leaveRoomButton = "id:Leave";

	public static String leaveRoomLabel = "xpath://XCUIElementTypeStaticText[@name='Leave room?']";
	public static String archievetheRoomInstead = "id:Your room chat history will be wiped away. To keep these chats, archive the room instead.";
	public static String archiveButton = "id:Archive";
	public static String leaveRoom = "xpath://XCUIElementTypeButton[@name='Leave']";

	// click on leave
	public static String newAdminLabel = "id:Pick New Admin(s) Before You Leave";
	public static String selectUserForLeavechat = "xpath:(//XCUIElementTypeStaticText[@name='replace'])[2]";
	public static String leaveButton = "xpath://XCUIElementTypeButton[@name='Leave']";

	// chat setting members
	public static String makeAdminButton = "id:Make admin";
	public static String makeModeratorButton = "id:Make moderator";
	public static String viewProfileButton = "id:View profile";
	public static String kickButton = "id:Kick";
	public static String removeAsModeratorButton = "id:Remove as moderator";

	// verify moderator
	public static String moderatorStarLogo = "xpath:(//XCUIElementTypeCell[XCUIElementTypeStaticText[@name='replace']]//XCUIElementTypeStaticText)[2]";

	public ChatScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void navigateToChatRoom(String chatRoom) {
		AppLibrary.findElementForMobile(driver, clickOnParticularChatRoom.replace("Replace", chatRoom)).click();
	}

	public void closeScreen() throws Exception {
		boolean VerifyAddFriendLabel = AppLibrary.isElementPresent(driver, FriendsScreen.addFriendLabel);

		if (VerifyAddFriendLabel) {
			AppLibrary.clickMobileElement(driver, FriendsScreen.backButton);
		}

		boolean VerifyChooseFriendLabel = AppLibrary.isElementPresent(driver, chooseFriendLabel);
		if (VerifyChooseFriendLabel) {
			AppLibrary.clickMobileElement(driver, FriendsScreen.cancelButton);
		}
	}

	public ChatScreen VerifyChooseFriAndAddFriendScreen(String user) throws Exception {
		new FriendsScreen(appLibrary).friendsScreenUi(user);
		AppLibrary.clickMobileElement(driver, FriendsScreen.backButton);
		ChooseFriendScreenUi();
		AppLibrary.clickMobileElement(driver, FriendsScreen.cancelButton);
		return new ChatScreen(appLibrary);

	}

	public ChatScreen ChooseFriendScreenUi() {
		AppLibrary.verifyMobileElement(driver, cancelButton, true);
		AppLibrary.verifyMobileElement(driver, chooseFriendsLabel);
		AppLibrary.verifyMobileElement(driver, doneButton, true);
		AppLibrary.verifyMobileElement(driver, addFriendLabel, true);
		AppLibrary.verifyMobileElement(driver, addFriendLogo, true);
		AppLibrary.verifyMobileElement(driver, addFriendLabel, true);
		return new ChatScreen(appLibrary);
	}

	public ChatScreen CreateChat(String user) throws Exception {
		AppLibrary.clickMobileElement(driver, addButton);
		new FriendsScreen(appLibrary).InviteFriend(user);

		return new ChatScreen(appLibrary);
	}

	public void createChatRoomBySearchingFriend(String friendName, String RoomName) throws Exception {
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, addButton).click();
		AppLibrary.findElementForMobile(driver, addFriendLabel).click();
		AppLibrary.enterMobileText(driver, FriendsScreen.matrixIdInput, friendName);
		AppLibrary.findElementForMobile(driver, FriendsScreen.addFriendButton).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, moreInfoButton).click();
		AppLibrary.findElementForMobile(driver, groupSubjecteEditButton).click();
		AppLibrary.enterMobileText(driver, grouptextField, RoomName);
		AppLibrary.findElementForMobile(driver, okButton).click();
		AppLibrary.clickMobileElement(driver, backButton);
		AppLibrary.clickMobileElement(driver, done);

	}

	public void joinChatGroups(String user1, String user2) throws Exception {
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, chatGroups.replace("user1", user1).replace("user2", user2)).click();
		AppLibrary.findElementForMobile(driver, joinGroup).click();
		AppLibrary.clickMobileElement(driver, done);
	}

	public void verifyChatRoom(String RoomName) throws Exception {
		AppLibrary.findElementForMobile(driver, chatRoomName.replace("replace", RoomName));
	}

	public void leaveRoom(String chatGroupName, String user) throws Exception {
		navigateToChatRoom(chatGroupName);
		AppLibrary.findElementForMobile(driver, moreInfoButton).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, leaveRoomButton).click();
		AppLibrary.verifyText(driver, archievetheRoomInstead,
				"Your room chat history will be wiped away. To keep these chats, archive the room instead.");
		AppLibrary.verifyText(driver, leaveRoomLabel, "Leave room?");

		AppLibrary.findElementForMobile(driver, cancelButton).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, leaveRoomButton).click();
		AppLibrary.verifyText(driver, archievetheRoomInstead,
				"Your room chat history will be wiped away. To keep these chats, archive the room instead.");
		AppLibrary.verifyText(driver, leaveRoomLabel, "Leave room?");

		AppLibrary.findElementForMobile(driver, leaveRoom).click();
		AppLibrary.sleep(2000);
		if (!user.equalsIgnoreCase("")) {
			AppLibrary.verifyText(driver, newAdminLabel, "Pick New Admin(s) Before You Leave");
			AppLibrary.clickMobileElement(driver, selectUserForLeavechat.replace("replace", user));
			AppLibrary.sleep(2000);
			AppLibrary.clickMobileElement(driver, leaveButton);
		}
		AppLibrary.sleep(3000);
		verifyChatRoomAbsence(chatGroupName);
	}

	public void verifyChatRoomAbsence(String RoomName) {
		AppLibrary.verifyAbsent(driver, chatRoomName.replace("replace", RoomName));
	}

	public ChatScreen chatScreenUi(String RoomName, String user1, String user2) throws Exception {

		AppLibrary.verifyMobileElement(driver, chatRoomUserLabel.replace("Replace", RoomName), true);
		AppLibrary.verifyMobileElement(driver, done, true);
		AppLibrary.verifyMobileElement(driver, message.replace("user1", user1).replace("user2", user2), true);
		AppLibrary.verifyMobileElement(driver, chatroomAddButton, true);
		AppLibrary.verifyMobileElement(driver, audioButton, true);
		AppLibrary.verifyMobileElement(driver, chatboxtext, true);
		AppLibrary.verifyMobileElement(driver, moreInfoButton, true);
		// click on more info button

		AppLibrary.clickMobileElement(driver, moreInfoButton);

		AppLibrary.verifyMobileElement(driver, settingLabel, true);
		AppLibrary.verifyMobileElement(driver, img, true);
		AppLibrary.verifyMobileElement(driver, groupSubjecteEditButton, true);
		AppLibrary.verifyMobileElement(driver, roomTopicEditButton, true);
		AppLibrary.verifyMobileElement(driver, addFriendsButton, true);
		AppLibrary.verifyMobileElement(driver, shareButton, true);
		AppLibrary.verifyMobileElement(driver, notificationLabel, true);
		AppLibrary.verifyMobileElement(driver, notificationToggle, true);
		AppLibrary.verifyMobileElement(driver, memberslabel, true);
		AppLibrary.verifyMobileElement(driver, userStatus.replace("user1", user1), true);
		AppLibrary.verifyMobileElement(driver, onlinLabel, true);
		AppLibrary.verifyMobileElement(driver, user2Label.replace("user2", user2), true);
		AppLibrary.verifyMobileElement(driver, invitedLabel, true);
		AppLibrary.verifyMobileElement(driver, leaveRoomButton, true);
		return new ChatScreen(appLibrary);

	}

	public void archiveRoom(String chatGroupName, String user) throws Exception {
		navigateToChatRoom(chatGroupName);
		AppLibrary.findElementForMobile(driver, moreInfoButton).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, leaveRoomButton).click();
		AppLibrary.verifyText(driver, archievetheRoomInstead,
				"Your room chat history will be wiped away. To keep these chats, archive the room instead.");
		AppLibrary.verifyText(driver, leaveRoomLabel, "Leave room?");

		AppLibrary.findElementForMobile(driver, archiveChat).click();
		AppLibrary.sleep(2000);
		verifyArchiveRoom(chatGroupName);
	}

	public void verifyArchiveRoom(String RoomName) throws Exception {
		AppLibrary.verifyMobileElement(driver, activeChat);
		AppLibrary.verifyMobileElement(driver, archiveChat);
		AppLibrary.clickMobileElement(driver, archiveChat);
		AppLibrary.findElementForMobile(driver, chatRoomName.replace("replace", RoomName));

	}

	public void makeUserModerator(String user2) throws Exception {
		AppLibrary.clickMobileElement(driver, user2Label.replace("user2", user2));
		AppLibrary.clickMobileElement(driver, makeModeratorButton);
		AppLibrary.verifyMobileElement(driver, moderatorStarLogo.replace("replace", user2), true);
	}

	public void RemoveUserAsModerator(String user2) throws Exception {
		AppLibrary.clickMobileElement(driver, user2Label.replace("user2", user2));
		AppLibrary.clickMobileElement(driver, removeAsModeratorButton);

		AppLibrary.sleep(5000);
		AppLibrary.verifyAbsent(driver, moderatorStarLogo.replace("replace", user2));
	}

	public void makeUserAdmin(String user2) throws Exception {
		AppLibrary.clickMobileElement(driver, user2Label.replace("user2", user2));
		AppLibrary.clickMobileElement(driver, makeAdminButton);
		AppLibrary.verifyAbsent(driver, moderatorStarLogo);
	}

	public void viewProfile(String user2) throws Exception {

	}

	public void kickUserFromChat(String user2) throws Exception {

	}

}
