#----------------------------START----------------------------
timestampVal=""

timestamp() {
	timestampVal=$(date "+%Y%m%d%H%M%S")
}

timestamp


platform=$1
suiteName=$2
osversion1=$3
osversion2=$4
device1=$5
device2=$6

echo "------------------Printing configurations Start------------------"
echo "platform:"$platform
echo "suiteName:"$suiteName
echo "OS1:"$osversion1
echo "OS2:"$osversion2
echo "Device1:"$device1
echo "Device2:"$device2
echo "BSUser:"$bsUserName
echo "BSkey:"$bsToken
echo "AppUrl:"$appUrl
echo "------------------Printing configurations End------------------"


echo "------------------Verify configurations Start------------------"
if [$platform == ""] || [$suiteName == ""] || [$bsUserName == ""] || [$bsToken == ""] || [$osversion1 == ""] || [$osversion2 == ""] || [$device1 == ""] || [$device2 == ""]
then
echo "Incomplete Parameters: One of the mandatory parameters are not present"
exit 1
fi
echo "------------------Verify configurations End------------------"


echo "------------------Uploading app file to BrowseeStack Start------------------"
if [ $1 == "android" ]
then
	appFilePath="Keanu_Android_Automation/resources/Keanu-0.0.4.2-primary-universal-debug.apk"
	appCustomID="keanuandroid$timestampVal"
else
	appFilePath="Keanu_iOS_Automation/resources/"
	appCustomID="keanuios$timestampVal"
fi

echo AppfilePath:${appFilePath}
echo AppCustomID:${appCustomID}

response=$(curl -u "${bsUserName}:${bsToken}" -X POST "https://api-cloud.browserstack.com/app-automate/upload" -F "file=@${appFilePath}" -F "data={\"custom_id\": \"${appCustomID}\"}")
echo Response:$response

appUrl=$(echo $response | jq --raw-output '.app_url')
echo $appUrl
echo "------------------Uploading app file to BrowseeStack End------------------"


echo "------------------Running Tests Starts------------------"
if [ $1 == "android" ]
then
	echo "I am in android"
	cd  Keanu_Android_Automation
else
	echo "I am in ios"
	cd Keanu_iOS_Automation
fi


echo "Runnign test suite with below command"
echo mvn clean test -Dsurefire.suiteXmlFiles="$suiteName" -DisBrowserstack.execution=true -DOS.name="android"  -Dbrowserstack.username="$bsUserName" -Dbrowserstack.authkey="$bsToken" -Dos.version1="$osversion1" -Dos.version2="$osversion2" -Ddevice.name1="$device1" -Ddevice.name2="$device2" -DappUrl="${appUrl}"


mvn clean test -Dsurefire.suiteXmlFiles="$suiteName" -DisBrowserstack.execution=true -DOS.name="android"  -Dbrowserstack.username="$bsUserName" -Dbrowserstack.authkey="$bsToken" -Dos.version1="$osversion1" -Dos.version2="$osversion2" -Ddevice.name1="$device1" -Ddevice.name2="$device2" -DappUrl="$appUrl"

echo "------------------Running Tests Ends------------------"